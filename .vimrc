filetype off
filetype plugin indent off
filetype plugin indent on
syntax on
colorscheme desert
execute pathogen#infect()
autocmd FileType go autocmd BufWritePre <buffer> Fmt
